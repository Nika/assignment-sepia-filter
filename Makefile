NAME := image-transformer

CC = clang
LINKER = $(CC)
NASM = nasm
NASMFLAGS = -felf64 -g
RM = rm -rf

BUILDDIR = build
SOLUTION_DIR = solution
TESTDIR = test_sepia

SRCDIR = $(SOLUTION_DIR)/src
INCDIR = $(SOLUTION_DIR)/include
OBJDIR = obj/$(SOLUTION_DIR)
TESTSRCDIR = $(TESTDIR)/src
OBJTESTDIR = obj/$(TESTDIR)

CFLAGS += $(strip $(file < $(SOLUTION_DIR)/compile_flags.txt)) -I$(INCDIR) 

all: $(OBJDIR)/image_filter_sepia_asm.o $(OBJDIR)/file_service.o $(OBJDIR)/inner_format.o $(OBJDIR)/image_filter_sepia.o $(OBJDIR)/image_rotation.o $(OBJDIR)/io_bmp.o $(OBJDIR)/main.o
	$(CC) -o $(BUILDDIR)/image_program $^

build:
	mkdir -p $(BUILDDIR)

$(OBJDIR)/inner_format.o: $(SRCDIR)/inner_format.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/image_filter_sepia_asm.o: $(SRCDIR)/image_filter_sepia_asm.asm build
	$(NASM) $(NASMFLAGS) $< -o $@

$(OBJDIR)/image_filter_sepia.o: $(SRCDIR)/image_filter_sepia.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/image_rotation.o: $(SRCDIR)/image_rotation.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/io_bmp.o: $(SRCDIR)/io_bmp.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/file_service.o: $(SRCDIR)/file_service.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJTESTDIR)/test.o: $(TESTSRCDIR)/test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJTESTDIR)/main.o: $(TESTSRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

test: $(OBJTESTDIR)/main.o $(OBJTESTDIR)/test.o $(OBJDIR)/image_filter_sepia_asm.o $(OBJDIR)/file_service.o $(OBJDIR)/inner_format.o $(OBJDIR)/image_filter_sepia.o $(OBJDIR)/io_bmp.o
	 $(CC) -o $(BUILDDIR)/image_test $^
	./$(BUILDDIR)/image_test $(TESTDIR)/1/input.bmp $(TESTDIR)/1/output_c.bmp $(TESTDIR)/1/output_asm.bmp

clean:
	$(RM) $(OBJDIR) $(BUILDDIR)
