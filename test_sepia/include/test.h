#ifndef TEST
#define TEST
#include <stdio.h>

void test_average ( struct image image);

void test_asm (struct image* image, FILE* output_file);

void test_c (struct image* image, FILE* output_file);
#endif

