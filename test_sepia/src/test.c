#include <image_filter_sepia.h>
#include <io_bmp.h>
#include <inner_format.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

static const char* write_status_msg[] = {
    [WRITE_OK] = "Write successful\n",
    [WRITE_ERR] = "Write unsuccessful\n"
};

static void print_msg (const char* msg){
    fprintf(stderr, "%s", msg);
}

void test_asm (struct image* image, FILE* output_file){

    print_msg("\nTest 'asm' starts:\n");

    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    sepia_asm_inplace(image);
             
    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
    end.tv_usec - start.tv_usec;
    printf( "Time elapsed in microseconds for asm test: %ld\n", res );

    enum write_status write_status = to_bmp(output_file, image);
                           
    print_msg(write_status_msg[write_status]);
    free_data(image);         
}

void test_c (struct image* image, FILE* output_file){

    print_msg("\nTest 'c' starts:\n");

    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    sepia_c_inplace(image);
             
    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
    end.tv_usec - start.tv_usec;
    printf( "Time elapsed in microseconds for c test: %ld\n", res );  

    enum write_status write_status = to_bmp(output_file, image);
                           
    print_msg(write_status_msg[write_status]);
    free_data(image);        
}

void test_average ( struct image image) {

    print_msg("\nTest 'average time' starts:\n");

    // for 'c' sepia 
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;
    struct image dop_image;

    for (size_t i = 0; i < 15; i++) {
        dop_image = image_create_copy(image);
        sepia_c_inplace(&dop_image);
    }
             
    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
    end.tv_usec - start.tv_usec;
    printf( "Time elapsed in microseconds for c test (average): %ld\n", res/15 );  

    // for 'asm' sepia 
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    for (size_t i = 0; i < 15; i++) {
        dop_image = image_create_copy(image);
        sepia_asm_inplace(&dop_image);
    }
             
    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    res = ((end.tv_sec - start.tv_sec) * 1000000L) +
    end.tv_usec - start.tv_usec;
    printf( "Time elapsed in microseconds for asm test (average): %ld\n", res/15 );  

    free_data(&dop_image);
    free_data(&image);
}

