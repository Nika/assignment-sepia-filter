#include <file_service.h>
#include <io_bmp.h>
#include "../include/test.h"
#include <string.h>


static const char* read_status_msg[] = {
    [READ_OK] = "Read successful\n",
    [READ_ERR] = "Read unsuccessful\n",
    [READ_MALLOC_ERR] = "Malloc error\n",
    [READ_INVALID_SIGNATURE] = "Invalid signature\n",
    [READ_INVALID_BITS] = "Invalid bits\n",
    [READ_INVALID_HEADER_SIZE] = "Invalid size if header\n",
    [READ_INVALID_PLANES] = "Invalid planes\n",
    [READ_INVALID_SIZE] = "Invalid size of image\n",
    [READ_FSEEK_ERR] = "Fseek error\n"
};


static const char* open_status_msg[] = {
    [OPEN_OK] = "Open successful\n",
    [OPEN_ERR] = "Open unsuccessful\n"
};

static void print_msg (const char* msg){
    fprintf(stderr, "%s", msg);
}

int main (int argc, char** argv){

  if (argc == 4) { 

    FILE* input_file = NULL;
    FILE* output_asm_file = NULL;
    FILE* output_c_file = NULL;
    enum open_status open_status = open_file_for_reading(argv[1], &input_file);
    print_msg(open_status_msg[open_status]);

    if (open_status == OPEN_OK) {
        open_status = open_file_for_writing(argv[2], &output_c_file);
        print_msg(open_status_msg[open_status]);

        if (open_status == OPEN_OK) {
            open_status = open_file_for_writing(argv[3], &output_asm_file);
            print_msg(open_status_msg[open_status]);

            if (open_status == OPEN_OK) {

                struct image image_c = image_create_empty();
                enum read_status read_status = from_bmp(input_file, &image_c); 
                print_msg(read_status_msg[read_status]); 
                
                if (read_status == READ_OK) {
                    struct image image_asm = image_create_copy(image_c);
                    struct image image_avg = image_create_copy(image_c);

                    test_c(&image_c, output_c_file);
                    test_asm(&image_asm, output_asm_file);
                    test_average(image_avg);
                }
                fclose(output_asm_file);
            }
            fclose(input_file);
            fclose(output_c_file);

        } else fclose(input_file);   
    } 

  } else print_msg("Not enough arguments!");

  return 0;
}

