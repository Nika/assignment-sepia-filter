#ifndef IO_BMP
#define IO_BMP

#include <inner_format.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

enum read_status  {
  READ_OK = 0,
  READ_ERR,
  READ_MALLOC_ERR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER_SIZE,
  READ_INVALID_PLANES,
  READ_INVALID_SIZE,
  READ_FSEEK_ERR
  };
 
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERR
};
enum read_status from_bmp( FILE* in, struct image* img );


enum write_status to_bmp( FILE* out, struct image const* img );
#endif

