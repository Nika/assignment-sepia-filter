#ifndef IMAGE_FILTER_SEPIA
#define IMAGE_FILTER_SEPIA

#include <inner_format.h>

void sepia_c_inplace( struct image* img );
void sepia_asm_inplace( struct image* img );
#endif

