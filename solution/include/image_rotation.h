#ifndef IMAGE_ROTATION
#define IMAGE_ROTATION
#include <inner_format.h>

struct image rotate( struct image const source );
#endif

