#include <io_bmp.h>

static uint16_t FILE_TYPE = 0x4d42;
static uint32_t DIB_SIZE = 40;
static uint32_t HEADER_SIZE = 54;
static uint16_t PLANES = 1;
static uint16_t BIT_COUNT = 24;

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

static uint32_t get_padding(uint32_t width) {

    const uint32_t width_in_bytes = width * 3;
    if (width_in_bytes % 4 == 0) return 0;
    return 4 - width_in_bytes % 4;

}

static uint32_t get_data_size(uint32_t width, uint32_t height, uint32_t padding) {

    return width*height*3 + padding*height;

}

static enum read_status read_header(FILE* file, struct bmp_header* header, uint32_t count) {

    size_t header_count = fread(header, sizeof(struct bmp_header), count, file);
    if (header_count != count) return READ_ERR;
    return READ_OK;

}

static enum read_status validate_header (struct bmp_header* header) {

    if (header->bfType != FILE_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->bOffBits != HEADER_SIZE) {
        return READ_INVALID_HEADER_SIZE;
    }
    if (header->biPlanes != PLANES) {
        return READ_INVALID_PLANES;
    }
    if (header->biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }
    return READ_OK;

}


static enum read_status read_data(FILE* file, struct image* img) {
    const uint32_t width = (uint32_t) img->width;
    const uint32_t height = (uint32_t) img->height;
    const uint32_t padding = get_padding(width);

    if (width == 0 || height == 0) return READ_INVALID_SIZE;

    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (data == NULL) return READ_MALLOC_ERR;
    struct pixel* pointer = data;
    for (size_t i = 0; i < height; i++) {
      size_t pixels_count = fread( pointer, sizeof(struct pixel), width, file);
      if (pixels_count != width) {
        free(pointer);
        return READ_ERR;
      }
      size_t padding_skip = fseek(file, padding, SEEK_CUR);
      if (padding_skip != 0) {
        free(pointer);
        return READ_FSEEK_ERR;
      }
      pointer = pointer + width;
    }
    img->data = data;

    return READ_OK;

}

static enum write_status write_header( FILE* file, uint32_t width, uint32_t height, uint32_t padding) {

    const uint32_t data_size = get_data_size(width, height, padding);
    struct bmp_header header = {
        .bfType = FILE_TYPE,
        .bfileSize = HEADER_SIZE + data_size,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = DIB_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = data_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    
    size_t header_count = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (header_count != 1) return WRITE_ERR;
    return WRITE_OK;

}

static enum write_status write_data(FILE* file, uint32_t width, uint32_t height, uint32_t padding, struct pixel* pointer) {

    for (size_t i = 0; i < height; i++) {
      size_t pixels_count = fwrite(pointer, sizeof(struct pixel), width, file);
      if (pixels_count != width) return WRITE_ERR;
      size_t padding_count = fwrite(pointer, padding, 1, file);
      if (padding_count != 1) return WRITE_ERR;
      pointer = pointer + width;  
    }
    return WRITE_OK;

}

enum read_status from_bmp( FILE* in, struct image* img ){

    struct bmp_header header;
    enum read_status header_status = read_header(in, &header, 1);
    if (header_status != READ_OK) return header_status;

     validate_header(&header);
    //enum read_status validate_status = validate_header(&header);
    //if (validate_status != READ_OK) return validate_status;

    img->width = (uint64_t) header.biWidth;
    img->height = (uint64_t) header.biHeight;
    enum read_status data_status = read_data(in, img);
    return data_status;

}

enum write_status to_bmp( FILE* out, struct image const* img ) {
   
    const uint32_t height = (uint32_t) image_get_height(img);
    const uint32_t width = (uint32_t) image_get_width(img);
    const uint32_t padding = (uint32_t) get_padding(width);
    enum write_status write_status = write_header(out, width, height, padding);
    if (write_status == WRITE_OK) {
      write_status = write_data(out, width, height, padding, img->data);
    }
    return write_status;

}

