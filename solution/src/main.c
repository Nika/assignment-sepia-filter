#include <file_service.h>
#include <image_filter_sepia.h>
#include <image_rotation.h>
#include <io_bmp.h>
#include <string.h>

static const char* open_status_msg[] = {
    [OPEN_OK] = "Open successful\n",
    [OPEN_ERR] = "Open unsuccessful\n"
};

static const char* read_status_msg[] = {
    [READ_OK] = "Read successful\n",
    [READ_ERR] = "Read unsuccessful\n",
    [READ_MALLOC_ERR] = "Malloc error\n",
    [READ_INVALID_SIGNATURE] = "Invalid signature\n",
    [READ_INVALID_BITS] = "Invalid bits\n",
    [READ_INVALID_HEADER_SIZE] = "Invalid size if header\n",
    [READ_INVALID_PLANES] = "Invalid planes\n",
    [READ_INVALID_SIZE] = "Invalid size of image\n",
    [READ_FSEEK_ERR] = "Fseek error\n"
};

static const char* write_status_msg[] = {
    [WRITE_OK] = "Write successful\n",
    [WRITE_ERR] = "Write unsuccessful\n"
};

static void print_msg (const char* msg){
    fprintf(stderr, "%s", msg);
}

int main( int argc, char** argv ) {

    if (argc == 4) {

        FILE* input_file = NULL;
        FILE* output_file = NULL;

        enum open_status open_status = open_file_for_reading(argv[1], &input_file);
        print_msg(open_status_msg[open_status]);

        if (open_status == OPEN_OK) {
             open_status = open_file_for_writing(argv[2], &output_file);
             print_msg(open_status_msg[open_status]);

             if (open_status == OPEN_OK) {
                struct image image = image_create_empty();
                enum read_status read_status = from_bmp(input_file, &image); 
                print_msg(read_status_msg[read_status]); 
    
                if (read_status == READ_OK) {
                    enum write_status write_status;

                    if (strcmp( argv[3], "c") == 0) {
                        sepia_c_inplace(&image);
                        write_status = to_bmp(output_file, &image);
                    } else if (strcmp(argv[3], "asm") == 0) {
                        sepia_asm_inplace(&image);
                        write_status = to_bmp(output_file, &image);
                    } else {
                         print_msg("Wrong last argument!");
                         return 0;
                    }
                    print_msg(write_status_msg[write_status]);
                    free_data(&image); 
                   
                    fclose(input_file);
                    fclose(output_file);
                 }
             } else fclose(input_file);
        }

    } else print_msg("Not enough arguments!");

    return 0;
}

