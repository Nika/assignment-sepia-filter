#include <inner_format.h>
#include <inttypes.h>
#include <stdio.h>

extern void sepia_asm(struct pixel * old_data, uint64_t size, struct pixel * new_data);

static unsigned char sat( uint64_t x) {
 if (x < 256) return x; return 255;
}

static struct pixel* pixel_of( struct image* img, uint64_t x, uint64_t y) {
 return &(img->data[y * img->width + x]);
}

float byte_to_float[256];

static void init_byte_to_float (){
     for (int i = 0; i < 256; i++) {
          byte_to_float[i] = (float) i;
     }
}

static void sepia_one( struct pixel* const pixel ) {
     static const float c[3][3] = {
     { .393f, .769f, .189f },
     { .349f, .686f, .168f },
     { .272f, .543f, .131f } };
    struct pixel const old = *pixel;
    pixel->r = sat( (uint64_t)((byte_to_float[old.r]) * c[0][0] + (byte_to_float[old.g]) * c[0][1] 
     + (byte_to_float[old.b]) * c[0][2]));
    pixel->g = sat( (uint64_t)((byte_to_float[old.r]) * c[1][0] + (byte_to_float[old.g]) * c[1][1] 
     + (byte_to_float[old.b]) * c[1][2]));
    pixel->b = sat( (uint64_t)((byte_to_float[old.r]) * c[2][0] + (byte_to_float[old.g]) * c[2][1] 
     + (byte_to_float[old.b]) * c[2][2]));
}

void sepia_c_inplace( struct image* img ) {
     init_byte_to_float();
     uint32_t x,y;

     for( y = 0; y < img->height; y++ )
     for( x = 0; x < img->width; x++ )
          sepia_one( pixel_of( img, x, y ) );
}

void sepia_asm_inplace( struct image* img ) {

     size_t size = img->width * img->height;
     size_t size_for_sse = size - size % 4;

     struct image image_old = image_create_copy(*img); 
     sepia_asm(image_old.data, size_for_sse * 3, img->data);

     for ( size_t i = 0; i < size % 4; i++ ) {
          sepia_one(&img->data[size_for_sse + i]);
     }

}
