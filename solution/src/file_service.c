#include "file_service.h"

enum open_status open_file_for_reading(char* file_path, FILE** file){
	*file = fopen(file_path, "rb");
	if (*file == NULL) return OPEN_ERR;
	return OPEN_OK;
} 

enum open_status open_file_for_writing(char* file_path, FILE** file){
	*file = fopen(file_path, "wb");
	if (*file == NULL) return OPEN_ERR;
	return OPEN_OK;
}

