#include "inner_format.h"
#include <string.h>

struct image image_create(uint64_t width, uint64_t height, struct pixel* data) {
  return (struct image){.width = width, .height = height, .data = data}; 
}

struct image image_create_empty() {
  return (struct image){.width = 0, .height = 0, .data = NULL}; 
}

struct image image_create_copy( struct image img ) {
  struct image image = image_create(img.width, img.height, NULL);
  size_t size = sizeof(struct pixel) * img.width * img.height;
  image.data = malloc(size);
  memcpy(image.data, img.data, size);
  return image; 
}

struct pixel* image_get_data(struct image const * img){
  return img->data;
}

uint64_t image_get_data_size(struct image const* img){
  return img->width * img->height;
}

uint64_t image_get_width(struct image const* img){
  return img->width;
}

uint64_t image_get_height(struct image const* img){
  return img->height;
}

void image_set_width(uint64_t width, struct image* img){
  img->width = width;
}

void image_set_height(uint64_t height, struct image* img){
  img->height = height;
}

void image_set_data(struct pixel* data, struct image* img){
  img->data = data;
}

void free_data(struct image* img) {
  free(img->data);
}

