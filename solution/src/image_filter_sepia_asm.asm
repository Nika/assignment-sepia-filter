section .data

align 16
c1: dd 0.131, 0.168, 0.189, 0.131

align 16
c2: dd 0.543, 0.686, 0.769, 0.543

align 16
c3: dd  0.272, 0.349, 0.393, 0.272

align 16
c4: dd 255.0, 255.0, 255.0, 255.0

global sepia_asm

section .text

%macro set_xmm_register 2

  pxor %1, %1 

  pinsrb %1, byte[%2 + 0], 0 
  pinsrb %1, byte[%2 + 1], 4 
  pinsrb %1, byte[%2 + 2], 8 
  pinsrb %1, byte[%2 + 3], 12

%endmacro

%macro take_new_numbers 2

  set_xmm_register xmm0, %1
  lea %1, [%1 + 1]

  set_xmm_register xmm1, %1
  lea %1, [%1 + 1]

  set_xmm_register xmm2, %1

  shufps xmm0, xmm0, %2
  shufps xmm1, xmm1, %2
  shufps xmm2, xmm2, %2

  cvtdq2ps xmm0, xmm0
  cvtdq2ps xmm1, xmm1
  cvtdq2ps xmm2, xmm2
  
  
%endmacro

; r8 - first of three (if number 1), 
; or second of three (if number 2),
; or third of three (if number 3) 
;
; rdi = old_data, rsi = size, rdx = new_data
sepia_asm:
  xor rcx, rcx
  xor r8, r8
  mov r8, 1

.loop:

  add rcx, 4
  push rsi
  push rdx
  push rdi
  push rcx
  mov rsi, r8
  call init_coef_and_numbers
  pop rcx
  pop rdi
  pop rdx
  pop rsi
  mov r8, rax

  mulps xmm3, xmm0
  mulps xmm4, xmm1
  mulps xmm5, xmm2

  addps xmm3, xmm4
  addps xmm3, xmm5
  
  push rsi
  push rdx
  push rdi
  push rcx
  call check_and_store
  pop rcx
  pop rdi
  pop rdx
  pop rsi

  lea rdi, [rdi + 3]
  lea rdx, [rdx + 4]

  cmp r8, 1
  jne .not_start_of_new_four_pixels
  lea rdi, [rdi + 3]

.not_start_of_new_four_pixels:  
  cmp rsi, rcx 
  ja .loop
  ret

check_and_store:
  pminsd xmm3, xmm6
  cvtps2dq xmm3, xmm3

  pextrb byte[rdx + 0], xmm3, 0 
  pextrb byte[rdx + 1], xmm3, 4
  pextrb byte[rdx + 2], xmm3, 8
  pextrb byte[rdx + 3], xmm3, 12
  ret

%macro change_coef 1

  shufps xmm3, xmm3, %1

  shufps xmm4, xmm4, %1

  shufps xmm5, xmm5, %1
  
%endmacro

;rsi == r8, rax - new value of r8
init_coef_and_numbers:
  movdqa xmm3, [c1]
  movdqa xmm4, [c2]
  movdqa xmm5, [c3]
  movdqa xmm6, [c4]

  cmp rsi, 1
  je .first
  cmp rsi, 2
  je .second
  cmp rsi, 3
  je .third
.first:
  change_coef 0xE7
  take_new_numbers rdi, 0xC0
  mov rax, 2
  ret
.second:
  change_coef 0x79
  take_new_numbers rdi, 0xF0
  mov rax, 3
  ret
.third:
  change_coef 0x9E
  take_new_numbers rdi, 0xFC
  mov rax, 1
  ret
